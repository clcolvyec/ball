#include <iostream>
#include <qpOASES.hpp>

using namespace std;
using namespace qpOASES;

int main() {
    // 定义问题的维度
    int nV = 2; // 变量数
    int nC = 2; // 约束数

    // 定义Q和c，用于目标函数
    real_t Q[4] = {2.0, 0.0, 0.0, 2.0}; // 对角阵形式
    real_t c[2] = {-2.0, -5.0};         // 线性项

    // 定义A和b，用于约束
    real_t A[4] = {1.0, 2.0, 2.0, 1.0};  // 约束矩阵
    real_t b[2] = {6.0, 6.0};            // 约束右侧常数

    // 定义边界
    real_t lb[2] = {0.0, 0.0};  // 下边界
    real_t ub[2] = {1.0, 1.0};  // 上边界

    // 创建一个 QProblem 对象
    QProblem exampleProblem(nV, nC);

    // 创建 Hessian 矩阵 Q，SymmetricMatrix 类型
    SymmetricMatrix H(nV);
    for (int i = 0; i < nV; i++) {
        for (int j = i; j < nV; j++) {
            H(i, j) = Q[i * nV + j];
            if (i != j) {
                H(j, i) = Q[i * nV + j];
            }
        }
    }

    // 创建约束矩阵 A
    Matrix A_matrix(nC, nV);
    for (int i = 0; i < nC; i++) {
        for (int j = 0; j < nV; j++) {
            A_matrix(i, j) = A[i * nV + j];
        }
    }

    // 创建 Bounds 和 Constraints
    Bounds bounds(nV);
    Constraints constraints(nC);

    for (int i = 0; i < nV; i++) {
        bounds.lower[i] = lb[i];
        bounds.upper[i] = ub[i];
    }

    for (int i = 0; i < nC; i++) {
        constraints.lower[i] = -qpOASES::INFTY;
        constraints.upper[i] = b[i];
    }

    // 初始化问题
    exampleProblem.init(H, c, A_matrix, bounds, constraints);

    // 求解优化问题
    real_t xOpt[2]; // 存储最优解
    exampleProblem.getPrimalSolution(xOpt);

    // 输出结果
    cout << "Optimal solution: " << endl;
    cout << "x1 = " << xOpt[0] << endl;
    cout << "x2 = " << xOpt[1] << endl;

    return 0;
}
