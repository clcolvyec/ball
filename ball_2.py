import numpy as np
import threading
import time
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# 参数定义
g = 9.81  # 重力加速度 (m/s^2)
K_D = 0.1  # 阻尼系数
delta_t = 0.002  # 数值计算时间步长 (s)
num_steps = int(1 / delta_t)  # 1秒的预测所需步数

# 初始条件
initial_position = np.array([0.0, 0.0, 0.0])  # 初始位置 [x, y, z] (m)
initial_velocity = np.array([10.0, 10.0, 10.0])  # 初始速度 [vx, vy, vz] (m/s)

# 预测结果共享变量
predicted_position = None
predicted_velocity = None
trajectory = []  # 存储位置轨迹

# 数值计算函数
def compute_acceleration(position, velocity):
    norm_b = np.linalg.norm(position)
    acceleration = np.zeros(3)  # 初始化三维加速度 [ax, ay, az]

    # x 和 y 方向只受阻尼力影响
    acceleration[0] = -K_D * norm_b * velocity[0]
    acceleration[1] = -K_D * norm_b * velocity[1]

    # z 方向受重力和阻尼力影响
    acceleration[2] = -g - K_D * norm_b * velocity[2]

    return acceleration

# 预测轨迹计算函数
def predict_trajectory(initial_position, initial_velocity):
    global predicted_position, predicted_velocity, trajectory

    # 初始化当前状态
    position = initial_position
    velocity = initial_velocity

    # 数值积分计算
    for _ in range(num_steps):
        acceleration = compute_acceleration(position, velocity)  # 计算加速度
        velocity = velocity + delta_t * acceleration  # 更新速度
        position = position + delta_t * velocity + 0.5 * delta_t**2 * acceleration  # 更新位置
        trajectory.append(position.copy())  # 记录位置

    # 保存预测结果
    predicted_position = position
    predicted_velocity = velocity

# 每20ms (50Hz) 的计算线程
def prediction_thread():
    global trajectory
    trajectory = []  # 重置轨迹记录

    while True:
        start_time = time.time()

        # 调用预测计算函数
        predict_trajectory(initial_position, initial_velocity)

        # 控制线程运行频率为50Hz
        elapsed_time = time.time() - start_time
        time.sleep(max(0, 1 / 50 - elapsed_time))  # 每20ms运行一次

# 3D 动态绘图线程
def plot_thread():
    global trajectory

    plt.ion()  # 打开交互模式
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # 绘制初始球体
    u = np.linspace(0, 2 * np.pi, 50)
    v = np.linspace(0, np.pi, 50)
    r = 1  # 球体半径
    x = r * np.outer(np.cos(u), np.sin(v))
    y = r * np.outer(np.sin(u), np.sin(v))
    z = r * np.outer(np.ones(np.size(u)), np.cos(v))
    ax.plot_surface(x, y, z, color='b', alpha=0.3)  # 球体表面

    while True:
        if trajectory:
            ax.clear()  # 清除先前图像

            # 重绘球体
            ax.plot_surface(x, y, z, color='b', alpha=0.3)

            # 提取轨迹
            traj = np.array(trajectory)
            ax.plot(traj[:, 0], traj[:, 1], traj[:, 2], color='r', label='Trajectory')  # 轨迹

            # 设置图形属性
            ax.set_xlim([-50, 50])
            ax.set_ylim([-50, 50])
            ax.set_zlim([-50, 50])
            ax.set_xlabel("X (m)")
            ax.set_ylabel("Y (m)")
            ax.set_zlabel("Z (m)")
            ax.set_title("3D Ball Trajectory")
            ax.legend()

            plt.pause(0.01)  # 更新图形

        time.sleep(0.05)  # 限制更新频率

# 启动计算线程
calc_thread = threading.Thread(target=prediction_thread, daemon=True)
calc_thread.start()

# 启动绘图线程
plot_thread = threading.Thread(target=plot_thread, daemon=True)
plot_thread.start()

# 主线程等待一段时间，观察结果
time.sleep(10)  # 运行10秒观察轨迹
